import { randomBytes } from 'react-native-randombytes';
import 'react-native-get-random-values';
import '@ethersproject/shims';
import { ethers, Wallet } from 'ethers';
import * as bip39 from "@scure/bip39";
import { HDNode } from 'ethers/lib/utils';

export const randomSeedPhrase = async () => {
    const bytes = await randomBytes(16)
    const mnemonic = ethers.utils.entropyToMnemonic(bytes);
    return mnemonic
}

export const walletFromSeed = async (mnemonic) => {
    const seed = bip39.mnemonicToSeedSync(mnemonic)
    const wallet = new Wallet(HDNode.fromSeed(seed).derivePath("m/44'/60'/0'/0/0"));
    return wallet
}